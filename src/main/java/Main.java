/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import com.demtechgroup.chvote.MessageUtils;
import com.demtechgroup.chvote.ProofMessage;
import com.demtechgroup.chvote.Verifier;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Main driver for proof verifier.
 * <p>
 * Usage: verify [-threads n|-locale locale] json_file_name
 * </p>
 */
public class Main {
  public static void main(String[] args)
      throws IOException {
    if (args.length != 2 && args.length != 4 && args.length != 6) {
      fail(MessageUtils.getMessage("Main.usage"));
    }

    if (!args[0].equalsIgnoreCase("verify")) {
      fail(MessageUtils.getMessage("Main.usage"));
    }

    File input = new File(args[args.length - 1]);

    if (!input.exists()) {
      fail(MessageUtils.getMessage("Main.file_not_exist_error", input.getName()));
    }

    if (!input.canRead()) {
      fail(MessageUtils.getMessage("Main.file_cannot_read_error", input.getName()));
    }

    ExecutorService threadPool = null;
    for (int i = 0; i < args.length - 2; i += 2) {
      if (args[i + 1].equals("-threads")) {
        int count = 0;
        try {
          count = Integer.parseInt(args[i + 2]);
        } catch (Exception e) {
          fail(MessageUtils.getMessage("Main.bad_thread_count_error"));
        }
        if (count <= 0) {
          fail(MessageUtils.getMessage("Main.bad_thread_count_error"));
        }
        threadPool = Executors.newFixedThreadPool(count);
      } else if (args[i + 1].equals("-locale")) {
        MessageUtils.setLocale(Locale.forLanguageTag(args[i + 2]));
      } else {
        fail(MessageUtils.getMessage("Main.unknown_option_error", args[i + 1]));
      }
    }


    Security.addProvider(new BouncyCastleProvider());

    Verifier verifier = new Verifier(threadPool);

    // RFC 7159  Section 8.1 default encoding is UTF-8.
    InputStreamReader jsonReader = new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8);

    List<ProofMessage> messages = verifier.process(input.getName(), jsonReader);

    for (ProofMessage message : messages) {
      System.out.println(message);
    }

    if (threadPool != null) {
      threadPool.shutdown();
    }
  }

  private static void fail(String message) {
    System.err.println(message);
    System.exit(1);
  }
}
