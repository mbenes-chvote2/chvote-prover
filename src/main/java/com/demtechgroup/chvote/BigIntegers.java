/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import java.math.BigInteger;

/**
 * A class of precomputed BigInteger constants.
 */
public class BigIntegers {
  public static final BigInteger ZERO  = BigInteger.ZERO;
  public static final BigInteger ONE   = BigInteger.ONE;
  public static final BigInteger TWO   = BigInteger.valueOf(2);
  public static final BigInteger THREE = BigInteger.valueOf(3);
  public static final BigInteger FOUR  = BigInteger.valueOf(4);
  public static final BigInteger FIVE  = BigInteger.valueOf(5);
  public static final BigInteger SIX   = BigInteger.valueOf(6);
  public static final BigInteger SEVEN = BigInteger.valueOf(7);
  public static final BigInteger EIGHT = BigInteger.valueOf(8);
  public static final BigInteger NINE  = BigInteger.valueOf(9);
  public static final BigInteger TEN   = BigInteger.TEN;

  private BigIntegers() {
  }

  /**
   * Return the passed in value as an unsigned byte array.
   *
   * @param value value to be converted.
   *
   * @return a byte array without a leading zero byte if present in the signed encoding.
   */
  public static byte[] asUnsignedByteArray(
      BigInteger value) {
    byte[] bytes = value.toByteArray();

    if (bytes[0] == 0) {
      byte[] tmp = new byte[bytes.length - 1];

      System.arraycopy(bytes, 1, tmp, 0, tmp.length);

      return tmp;
    }

    return bytes;
  }
}
