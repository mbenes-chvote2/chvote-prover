/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import com.demtechgroup.chvote.chport.service.model.DecryptionProof;
import com.demtechgroup.chvote.chport.service.model.Encryption;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Checker for off-line encryption proofs.
 */
public class DecryptionProofChecker {
  private final ArgumentValidator             validator;
  private final DecryptionProofHashCalculator hashCalc;
  private final BigInteger                    two_to_tau;
  private final HashCalculator                accumulator;
  private final ExecutorService               executorService;


  /**
   * Base constructor.
   *
   * @param validator       validator for proof values.
   * @param hashFactory     factory to provide hashes for computing challenges.
   * @param executorService thread pool for scheduling proofs.
   */
  public DecryptionProofChecker(ArgumentValidator validator, com.demtechgroup.chvote.HashFactory hashFactory,
                                ExecutorService executorService) {
    this.validator = validator;
    this.hashCalc = new DecryptionProofHashCalculator(hashFactory);
    this.executorService = executorService;
    this.two_to_tau = BigInteger.valueOf(2).pow(validator.getPublicParameters().getSecurityParameters().getTau());
    this.accumulator = hashFactory.createCalculator();

  }

  /**
   * Algorithm 7.50: CheckDecryptionProofs
   *
   * @param pi_prime decryption proofs.
   * @param pk       encryption key shares.
   * @param e        ElGamal encryptions
   * @param B_prime  partial decryptions
   *
   * @return true if all the proofs are valid, false otherwise
   */
  public Future<ProofResult> checkDecryptionProofs(List<DecryptionProof> pi_prime, List<BigInteger> pk,
                                                   List<Encryption> e, List<List<BigInteger>> B_prime) {
    // Size checks
    int s = validator.getPublicParameters().getS() + 1;     // +1 for election authority.
    int N = e.size();

    validator.assertTrue(pi_prime.size() == s, "DecryptionProofChecker.proof_count_wrong_error");
    validator.assertTrue(pk.size() == s, "DecryptionProofChecker.pk_share_count_wrong_error");
    validator.assertTrue(B_prime.size() == s, "DecryptionProofChecker.B_primes_row_count_wrong_error");
    for (List l : B_prime) {
      validator.assertTrue(l.size() == N, "DecryptionProofChecker.B_primes_columns_count_wrong_error");
    }

    // Validity checks
    validator.checkDecryptionProofValues(pi_prime, "DecryptionProofChecker.pi_prime_is_ts_error");
    validator.containsOnlyMembers(pk, "DecryptionProofChecker.key_shares_not_member_error");
    validator.containsOnlyMembers(e, "DecryptionProofChecker.encryptions_not_member_error");
    validator.containsOnlyMembers(B_prime, "DecryptionProofChecker.B_primes_not_member_error");

    return executorService.submit(new Callable<ProofResult>() {
      @Override
      public ProofResult call()
          throws Exception {
        // for j = 1...s, CheckDecryptionProof, return on false or completion
        for (int j = 0; j != s; j++) {
          if (!checkDecryptionProof(pi_prime.get(j), pk.get(j), e, B_prime.get(j))) {
            return new ProofResult(
                Timestamp.getTimestamp() + " " + MessageUtils.getMessage("DecryptionProofChecker.task_description"));
          }
        }

        return new ProofResult(
            Timestamp.getTimestamp() + " " + MessageUtils.getMessage("DecryptionProofChecker.task_description"),
            accumulator.getResult());
      }
    });
  }

  /**
   * Algorithm 7.51: CheckDecryptionProof
   *
   * @param pi_prime the decryption proof.
   * @param pk_j     encryption key share.
   * @param e        ElGamal encryptions.
   * @param b_prime  partial decryptions.
   *
   * @return true if the proof is valid, false otherwise
   */
  private boolean checkDecryptionProof(DecryptionProof pi_prime, BigInteger pk_j, List<Encryption> e,
                                       List<BigInteger> b_prime) {
    BigInteger p = validator.getPublicParameters().getEncryptionGroup().getP();
    BigInteger g = validator.getPublicParameters().getEncryptionGroup().getG();

    // c = getNIZKPChallenge((y = pkj, e(b), b'), t, tau)  (final value from tau pre-calculated in constructor)
    BigInteger c = getNIZKPChallenge(pk_j, e, b_prime, pi_prime.getT());

    accumulator.update(BigIntegers.asUnsignedByteArray(c));

    List<BigInteger> t_prime = new ArrayList<>(e.size() + 1);
    // t'0 <- pk_j**(-c) . g**s mod p
    t_prime.add(pk_j.modPow(c.negate(), p).multiply(g.modPow(pi_prime.getS(), p)).mod(p));
    // t'1...N  <- b(i)'**(-c) . e(i,b)**s mod p
    for (int i = 0; i != e.size(); i++) {
      t_prime.add(b_prime.get(i).modPow(c.negate(), p)
                         .multiply(e.get(i).getB().modPow(pi_prime.getS(), p)).mod(p));
    }

    // t0 == t'0 & t(1..N) == t'(1..N)
    boolean isProofValid = true;
    for (int i = 0; i != t_prime.size(); i++) {
      isProofValid &= (pi_prime.getT().get(i).compareTo(t_prime.get(i)) == 0);
    }

    return isProofValid;
  }

  /**
   * Algorithm 7.4: GetNIZKPChallenge (tau value missing as precalculated in constructor)
   *
   * @param pk_j    encryption key share.
   * @param e       ElGamal encryptions.
   * @param b_prime partial decryptions.
   * @param t       the commitments vector.
   *
   * @return the challenge.
   */
  private BigInteger getNIZKPChallenge(BigInteger pk_j, List<Encryption> e, List<BigInteger> b_prime,
                                       List<BigInteger> t) {
    return new BigInteger(1, hashCalc.calculateHash(pk_j, e, b_prime, t)).mod(two_to_tau);
  }
}
