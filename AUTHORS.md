# Authors

This tool has been developed by David Hook and Carsten Schürmann, as part of a
contract between the State of Geneva and DemTech Group.

Minor changes to adapt the parsing of the data to later developments have been
committed by Thomas Hofer.
