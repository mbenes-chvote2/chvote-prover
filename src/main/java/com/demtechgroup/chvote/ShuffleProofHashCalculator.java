/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import com.demtechgroup.chvote.chport.service.model.Encryption;
import com.demtechgroup.chvote.chport.service.model.ShuffleProof;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Hash calculation support for shuffle proofs - this is built on the original Hash class but targeted more specifically
 * towards the requirements for verifying shuffle proofs.
 */
public class ShuffleProofHashCalculator
    extends ProofHashCalculator {
  private static final Charset CONVERSION_CHARSET = Charset.forName("UTF-8");

  /**
   * Base constructor.
   *
   * @param hashFactory factory to provide HashCalculators for use with this class.
   */
  public ShuffleProofHashCalculator(com.demtechgroup.chvote.HashFactory hashFactory) {
    super(hashFactory);
  }

  /**
   * Calculate a challenge hash for a NIZKP for a shuffle proof.
   *
   * @param e        ElGamal encryptions.
   * @param e_prime  shuffled ElGamal encryptions.
   * @param pi_c     primary proof commitments.
   * @param pi_c_hat secondary proof commitments.
   * @param pk       public key value.
   * @param t        the proof commitments vector.
   *
   * @return the challenge hash, value composed of the arguments.
   */
  byte[] calculateHash(List<Encryption> e, List<Encryption> e_prime, List<BigInteger> pi_c, List<BigInteger>
      pi_c_hat, BigInteger pk, ShuffleProof.T t) {
    HashCalculator calculator = hashFactory.createCalculator();

    calculator.update(recHash_L(e, e_prime, pi_c, pi_c_hat, pk));
    calculator.update(recHash_L(t));

    return calculator.getResult();
  }

  /**
   * Calculate a hash towards a NIZKP for a shuffle proof as part of the collective challenges generator
   * in the ShuffleProofChecker.
   *
   * @param e       ElGamal encryptions.
   * @param e_prime shuffled ElGamal encryptions.
   * @param pi_c    proof commitments.
   *
   * @return the challenge hash value, composed of the arguments.
   */
  byte[] calculateHash(List<Encryption> e, List<Encryption> e_prime, List<BigInteger> pi_c) {
    HashCalculator calculator = hashFactory.createCalculator();

    calculator.update(recHash_L(e));
    calculator.update(recHash_L(e_prime));
    calculator.update(recHash_L(pi_c));

    return calculator.getResult();
  }

  /**
   * Return the hash of a BigInteger - also used for collective challenge generation.
   *
   * @param bigInteger the big integer to be hashed.
   *
   * @return the hash value.
   */
  byte[] calculateHash(BigInteger bigInteger) {
    return recHash_L(bigInteger);
  }

  /**
   * Calculate a hash for use in the independent generators algorithm.
   *
   * @param label1 distinguishing label for hash.
   * @param label2 second distinguishing label for hash.
   * @param i      primary seed.
   * @param x      secondary seed.
   *
   * @return the resultant hash.
   */
  byte[] calculateHash(String label1, String label2, BigInteger i, BigInteger x) {
    HashCalculator calculator = hashFactory.createCalculator();

    calculator.update(recHash_L(label1));
    calculator.update(recHash_L(label2));
    calculator.update(recHash_L(i));
    calculator.update(recHash_L(x));

    return calculator.getResult();
  }

  /*
   * Algorithm 4.9: RecHash_L, non-varargs version
   * <p>
   * Computes the hash value h(v_1, ..., v_k) \in B^L of multiple inputs v_1, ..., v_k in a recursive manner, taking
   * into
   * account the requirements of a shuffle proof calculator.
   * </p>
   * <p>
   * This method performs the necessary casts and conversions for the hashing to be compliant to the definition in
   * section 4.3.
   * </p>
   *
   * @param object the element which needs to be cast
   * @return the recursive hash as defined in section 4.3
   */
  protected byte[] recHash_L(Object object) {
    if (object instanceof String) {
      return hash_L(((String) object).getBytes(CONVERSION_CHARSET));
    } else if (object instanceof BigInteger) {
      return hash_L((BigInteger) object);
    } else if (object instanceof byte[]) {
      return hash_L((byte[]) object);
    } else if (object instanceof List) {
      return recHash_L((List) object);
    } else if (object instanceof Object[]) {
      return recHash_L((Object[]) object);
    } else if (object instanceof Encryption) {
      return recHash_L(((Encryption) object).elementsToHash());
    } else if (object instanceof ShuffleProof.T) {
      return recHash_L(((ShuffleProof.T) object).elementsToHash());
    } else {
      throw new IllegalArgumentException(MessageUtils.getMessage("HashCalculator.unknown_object_error")
                                         + ((object != null) ? object.getClass().getName() : "null"));
    }
  }
}
